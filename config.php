<?php

  if (isset ($_POST['caldera-ongkir-update-cost'])) {
    global $wpdb;
    $wpdb->query('TRUNCATE caldera_ongkir_cost');
  } else if (isset ($_POST['caldera-ongkir-submit'])) {
    $caldera_ongkir_config = array('apikey', 'origin-province', 'origin-city', 'origin-subdistrict');
    foreach ($caldera_ongkir_config as $input) {
      if (isset ($_POST['caldera-ongkir-' . $input])) update_option('caldera-ongkir-' . $input, $_POST['caldera-ongkir-' . $input]);
    }
  }

  global $wpdb;
  $config_province = $wpdb->get_row("SELECT * FROM caldera_ongkir_province WHERE province_id=" . get_option('caldera-ongkir-origin-province'));
  $config_province = is_null($config_province) ? json_decode('{"province_id": 0, "province": ""}') : $config_province;
  $config_city     = $wpdb->get_row("SELECT * FROM caldera_ongkir_city WHERE city_id=" . get_option('caldera-ongkir-origin-city'));
  $config_city     = empty ($config_city) ? json_decode('{"city_id": 0, "city_name": ""}') : $config_city;
  $config_subdistrict = $wpdb->get_row("SELECT * FROM caldera_ongkir_subdistrict WHERE subdistrict_id=" . get_option('caldera-ongkir-origin-subdistrict'));
  $config_subdistrict = empty ($config_subdistrict) ? json_decode('{"subdistrict_id": 0, "subdistrict_name": ""}') : $config_subdistrict;

?>
<div class="wrap">
  <H1>Caldera Ongkir Settings</H1>
  <form method="POST">
  <table class="form-table">

    <?php if ($_POST): ?>
    <caption>
      <div class="notice notice-success is-dismissible">
        <p>Changes Saved</p>
      </div>
    </caption>
    <?php endif ?>

    <tbody>

      <!-- apikey begin -->
      <tr>
        <th scope="row">
          <label for="caldera-ongkir-apikey">RajaOngkir APIKEY</label>
        </th>
        <td>
          <input name="caldera-ongkir-apikey" type="text" class="regular-text" value="<?= get_option('caldera-ongkir-apikey') ?>">
        </td>
      </tr>
      <!-- apikey end -->

      <!-- origin begin -->
      <tr>
        <th scope="row">
          <label for="caldera-ongkir-origin">Alamat Asal Pengiriman</label>
        </th>
        <td>
          <select name="caldera-ongkir-origin-province" class="caldera-ongkir-origin-province">
            <option value="<?= $config_province->province_id ?>"><?= $config_province->province ?></option>
          </select>
        </td>
      </tr>
      <tr>
        <th></th>
        <td>
          <select name="caldera-ongkir-origin-city" class="caldera-ongkir-origin-city">
            <option value="<?= $config_city->city_id ?>"><?= $config_city->city_name ?></option>
          </select>
        </td>
      </tr>
      <tr>
        <th></th>
        <td>
          <select name="caldera-ongkir-origin-subdistrict" class="caldera-ongkir-origin-subdistrict">
            <option value="<?= $config_subdistrict->subdistrict_id ?>"><?= $config_subdistrict->subdistrict_name ?></option>
          </select>
        </td>
      </tr>
      <!-- origin end -->

      <!-- submit begin -->
      <tr>
        <th scope="row"></th>
        <td>
          <input type="submit" name="caldera-ongkir-submit" class="button button-primary" value="Save Changes">
        </td>
      </tr>
      <!-- submit end -->

      <!-- update ongkir begin -->
      <tr>
        <th scope="row">
          <label for="caldera-ongkir-update-cost"></label>
        </th>
        <td>
          <input type="submit" name="caldera-ongkir-update-cost" class="button button-secondary" value="Update Ongkir">
          <br/>
          <small>* peringatan: update ongkir akan menghapus semua ongkos kirim<br/> yang telah didownload dari rajaongkir API</small>
        </td>
      </tr>
      <!-- update ongkir end -->

    </tbody>

  </table>
  </form>
</div>

<link rel="stylesheet" type="text/css" href="<?= CFCORE_URL . 'fields/select2/css/select2.css' ?>">
<script type="text/javascript" src="<?= CFCORE_URL . 'fields/select2/js/select2.min.js' ?>"></script>
<script type="text/javascript">
  jQuery(function () {
    jQuery('select[name^="caldera-ongkir-origin"]').select2({ width: '40%' })
    var caldera_ongkir_api_url     = '<?php echo CALDERA_ONGKIR_LOCAL_API ?>'
    var caldera_ongkir_province    = jQuery('[name="caldera-ongkir-origin-province"]')
    var caldera_ongkir_city        = jQuery('[name="caldera-ongkir-origin-city"]')
    var caldera_ongkir_subdistrict = jQuery('[name="caldera-ongkir-origin-subdistrict"]')
    drawProvinces()
    caldera_ongkir_province.change(drawCities)
    caldera_ongkir_city.change(drawSubDistricts)

    function drawProvinces () {
      jQuery.get(caldera_ongkir_api_url, {entity: 'province'}, function (provinces) {
        for (var p in provinces) {
          var prov = provinces[p]
          caldera_ongkir_province.append('<option value="'+prov.province_id+'">'+prov.province+'</option>')
        }
      })
    }

    function drawCities () {
      jQuery('div.caldera-ongkir-origin-city a.ccselect2-choice span.ccselect2-chosen').html('')
      caldera_ongkir_city.html('<option selected="selected"></option>')
      jQuery.get(caldera_ongkir_api_url, {
        entity: 'city', 
        province: caldera_ongkir_province.val()
      }, function (cities) {
        for (var p in cities) {
          var city = cities[p]
          caldera_ongkir_city.append('<option value="'+city.city_id+'">'+city.type+' '+city.city_name+'</option>')
        }
      })
    }

    function drawSubDistricts () {
      jQuery('div.caldera-ongkir-origin-subdistrict a.ccselect2-choice span.ccselect2-chosen').html('')
      caldera_ongkir_subdistrict.html('<option selected="selected"></option>')
      jQuery.get(caldera_ongkir_api_url, {
        entity: 'subdistrict', 
        city: caldera_ongkir_city.val()
      }, function (subdistricts) {
        for (var s in subdistricts) {
          var district = subdistricts[s]
          caldera_ongkir_subdistrict.append('<option value="'+district.subdistrict_id+'">'+district.subdistrict_name+'</option>')
        }
      })
    }

  })
</script>