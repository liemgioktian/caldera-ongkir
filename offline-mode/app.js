const fs        = require('fs')
const Sequelize = require('sequelize')
const sequelize = new Sequelize('mysql://root:root@localhost:3306/wordpress', {logging: false})
var lines       = fs.readFileSync('php').toString().split("\n")

var caldera_ongkir_subdistrict = sequelize.define('caldera_ongkir_subdistrict', {
  subdistrict_id: {
    type: Sequelize.INTEGER
  },
  subdistrict_name: {
    type: Sequelize.STRING
  },
  city_id: {
    type: Sequelize.INTEGER
  }
})

var caldera_ongkir_cost = sequelize.define('caldera_ongkir_cost', {
  origin: {
    type: Sequelize.INTEGER
  },
  destination: {
    type: Sequelize.INTEGER
  },
  courier: {
    type: Sequelize.STRING
  },
  service: {
    type: Sequelize.STRING
  },
  weight: {
    type: Sequelize.INTEGER
  },
  result: {
    type: Sequelize.TEXT
  }
})

var caldera_ongkir_city = sequelize.define('caldera_ongkir_city', {
  city_id: {
    type: Sequelize.INTEGER
  },
  city_name: {
    type: Sequelize.STRING
  },
  type: {
    type: Sequelize.STRING
  },
  province_id: {
    type: Sequelize.INTEGER
  }
})

return caldera_ongkir_city.sync({force: true})
.then(() => {
  return caldera_ongkir_subdistrict.sync({force: true})
})
.then(() => {
  return caldera_ongkir_cost.sync({force: true})
})
.then(() => {
  return main (1, 0)
})
.catch((e) => {
  console.error(e)
})

function main (linenumber, cityId) {
  console.log(linenumber)
  var line  = lines[linenumber]
  linenumber= linenumber + 1

  if (line === '<?php } ?>') 
  {
    return false
  }

  else if (line === '<option value="">--</option>' || '</select>' === line || line.length < 1)
  {
    main (linenumber, cityId)
    return false
  }

  else if (line.indexOf("if ($_GET['ch'] == '") > -1)
  {
    return getCity(line).then(city => {
      return caldera_ongkir_city.create({
        city_name: city.name,
        type: city.type,
        province_id: city.prov
      })
    }).then((created) => {
      main (linenumber, created.dataValues.id)
      return false
    })
  }

  else
  {
    var ongkos = 0
    getsubd (line).then(subd => {
      ongkos = subd.cost
      return caldera_ongkir_subdistrict.create({
        city_id: cityId,
        subdistrict_name: subd.name
      })
    }).then(created => {
      caldera_ongkir_cost.create({
        destination: created.dataValues.id,
        courier: 'jne',
        service: 'REG',
        weight: 1000,
        result: '[{"code":"jne","name":"Jalur Nugraha Ekakurir (JNE)","costs":[{"service":"OKE","description":"Ongkos Kirim Ekonomis","cost":[{"value":'+ongkos+',"etd":"2-3","note":""}]},{"service":"REG","description":"Layanan Reguler","cost":[{"value":'+ongkos+',"etd":"1-2","note":""}]},{"service":"YES","description":"Yakin Esok Sampai","cost":[{"value":'+ongkos+',"etd":"1-1","note":""}]}]}]'
      })
    }).then(() => {
      main (linenumber, cityId)
      return false
    })
  }
}

function getCity (line) {
  return new Promise ((res, rej) => {
    city = line.split("if ($_GET['ch'] == '")
    city = city[1].split("'")
    city = city[0]
    type = ''
    if (city.indexOf('KAB') > -1) type = 'KABUPATEN'
    else if (city.indexOf('KOTA') > -1)  type = 'KOTA'
    city = city
            .replace('KAB-','')
            .replace('KOTA-','')
            .replace('KAB.', '')
            .replace('KOTA.', '')
            .replace('ADMINISTRASI', '')
            .replace('-', '')
            .replace(' ', '')
    return getProvince(city).then(prov => {
      res({name: city, type: type, prov: prov})
    })
  })
}

function getProvince (city_name) {
  return new Promise ((res, rej) => {
    var split = []
    split = city_name.split('/')
    city_name = split[0]
    split = city_name.split('&')
    city_name = split[0]

    if ('TIDORE' === city_name) city_name += 'KEPULAUAN'
    if ('TANATORAJAUTARA' === city_name) city_name = city_name.replace('UTARA','')

    sequelize.query("SELECT * FROM `caldera_ongkir_city` WHERE \
      SUBSTRING_INDEX(REPLACE(REPLACE(REPLACE(`city_name`, '-', ''), 'kepulauan', ''), ' ', ''),'(', 1) = '"+city_name+"'\
      OR SUBSTRING_INDEX(REPLACE(REPLACE(REPLACE(`city_name`, '-', ''), 'kepulauan', ''), ' ', ''),'/', 1) = '"+city_name+"'")
    .then((found) => {
      res(found[0][0] ? found[0][0].province_id : 0)
    })
  })
}

function getsubd (line) {
  return new Promise ((res, rej) => {
    var kec   = line.split('">')
    var ongkos= kec[0].replace('<option value="', '')
    res({name: kec[1].replace('</option>', '').trim(), cost: ongkos})
  })
}

/*
insert(1, 0)
function insert (l, cityId) {
  console.log('l', l)
  if (lines[l] === '<?php } ?>') return false
  else {
    var line = lines[l]
    l = l + 1
  }

  if (line.indexOf("if ($_GET['ch'] == '") > -1) {
    city = line.split("if ($_GET['ch'] == '")
    city = city[1].split("'")
    city = city[0]
    type = city.indexOf('KOTA') ? 'KOTA' : 'KABUPATEN'
    city = city.replace('KAB-','').replace('KOTA-','')
    caldera_ongkir_city.create({
      city_name: city,
      type: type
    }).then(city => {
      insert (l, city.dataValues.id)
      return false
    })
  } 

  else if (line === '<option value="">--</option>' || '</select>' === line || line.length < 1) {
    insert (l, cityId)
    return false
  }

  else {
    var kec   = line.split('">')
    var ongkos= kec[0].replace('<option value="', '')
    caldera_ongkir_subdistrict.create({
      subdistrict_name: kec[1].replace('</option>', '').trim(),
      city_id: cityId
    }).then(kec => {
      return caldera_ongkir_cost.create({
        origin: 1512,
        destination: kec.dataValues.id,
        courier: 'jne',
        service: 'REG',
        weight: 1000,
        result: '[{"code":"jne","name":"Jalur Nugraha Ekakurir (JNE)","costs":[{"service":"OKE","description":"Ongkos Kirim Ekonomis","cost":[{"value":'+ongkos+',"etd":"2-3","note":""}]},{"service":"REG","description":"Layanan Reguler","cost":[{"value":'+ongkos+',"etd":"1-2","note":""}]},{"service":"YES","description":"Yakin Esok Sampai","cost":[{"value":'+ongkos+',"etd":"1-1","note":""}]}]}]'
      })
    }).then(() => {
      insert (l, cityId)
      return false
    })
  }

}

caldera_ongkir_city.findAll().then(cities => {
  pair (0, cities)
})

function pair (c, cities) {
  if (!cities[c]) return false
  var city_name = cities[c].dataValues.city_name
  var id = cities[c].dataValues.id
  c = c + 1
  sequelize.query("SELECT * FROM `caldera_ongkir_city` WHERE REPLACE(`city_name`, ' ', '') = '"+city_name+"'")
  .then((found) => {
    if (found[0][0]) {
      caldera_ongkir_city.update({province_id: found[0][0].province_id}, {where: {id: id}})
      .then(() => {
        pair (c, cities)
      })
    } else pair (c, cities)
  })
}
*/