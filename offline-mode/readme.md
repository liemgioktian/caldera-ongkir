1. install caldera, caldera ongkir, bikin form
2. preview form, run cubit.js d console
3. run app.js
4. export caldera_ongkir_cities & caldera_ongkir_subdistricts k csv, uncheck set column name as header
5. d csv, hapus kolom city_id/subdistrict_id, createdat, updatedat
6. update caldera_ongkir_costs, set createdat = '0000-00-00 00:00:00', updatedat jg
7. export caldera_ongkir_costs as sql
8. d sql tsb, hapus semua ,'createdat','updatedat'. hapus jg '0000-00-00 00:00:00'
9. truncate caldera_ongkir_city, caldera_ongkir_subdistrict, caldera_ongkir_cost
10. import 
  caldera_ongkir_cities.csv ke caldera_ongkir_city
  caldera_ongkir_subdistricts.csv ke caldera_ongkir_subdistrict
  caldera_ongkir_costs.sql ke caldera_ongkir_cost
11. masuk menu caldera ongkir config, set origin ke cempaka putih