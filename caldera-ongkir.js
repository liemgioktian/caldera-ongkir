jQuery(function () {
  var caldera_ongkir_cost       = jQuery('select.caldera_ongkir_cost')
  var caldera_ongkir_province   = jQuery('select.caldera_ongkir_province')
  var caldera_ongkir_city       = jQuery('select.caldera_ongkir_city')
  var caldera_ongkir_subdistrict= jQuery('select.caldera_ongkir_subdistrict')

  jQuery('div[class^="caldera_ongkir_"] select').select2()
  drawProvinces ();
  caldera_ongkir_province.change(drawCities)
  caldera_ongkir_city.change(drawSubDistricts)
  caldera_ongkir_subdistrict.change(calculateCost)

  function calculateCost () {
    caldera_ongkir_notif ('Mohon tunggu sejenak..')
    jQuery.get(caldera_ongkir_api_url, {
      entity: 'cost',
      subdistrict: caldera_ongkir_subdistrict.find(':selected').attr('data-value')
    }, function (cost) {
      if (cost.error) {
        caldera_ongkir_cost
        .html('<option value="0">0</option>')
        .trigger('change')
        caldera_ongkir_notif (cost.error)
      } else {
        caldera_ongkir_cost
        .html('<option value="'+cost.value+'">'+cost.value+'</option>')
        .trigger('change')
        caldera_ongkir_notif (null)        
      }
    })
  }

  function drawProvinces () {
    jQuery.get(caldera_ongkir_api_url, {entity: 'province'}, function (provinces) {
      for (var p in provinces) {
        var prov = provinces[p]
        caldera_ongkir_province.append('<option value="'+prov.province+'" data-value="'+prov.province_id+'">'+prov.province+'</option>')
      }
    })
  }

  function drawCities () {
    jQuery('div.caldera_ongkir_city a.ccselect2-choice span.ccselect2-chosen').html('')
    caldera_ongkir_city.html('<option selected="selected"></option>')
    jQuery.get(caldera_ongkir_api_url, {
      entity: 'city', 
      province: caldera_ongkir_province.find(':selected').attr('data-value')
    }, function (cities) {
      for (var p in cities) {
        var city = cities[p]
        caldera_ongkir_city.append('<option value="'+city.city_name+'" data-value="'+city.city_id+'">'+city.type+' '+city.city_name+'</option>')
        jQuery('div.caldera_ongkir_city_container').removeClass('hidden')
      }
    })
  }

  function drawSubDistricts () {
    jQuery('div.caldera_ongkir_subdistrict a.ccselect2-choice span.ccselect2-chosen').html('')
    caldera_ongkir_subdistrict.html('<option selected="selected"></option>')
    jQuery.get(caldera_ongkir_api_url, {
      entity: 'subdistrict', 
      city: caldera_ongkir_city.find(':selected').attr('data-value')
    }, function (subdistricts) {
      for (var s in subdistricts) {
        var district = subdistricts[s]
        caldera_ongkir_subdistrict.append('<option value="'+district.subdistrict_name+'" data-value="'+district.subdistrict_id+'">'+district.subdistrict_name+'</option>')
        jQuery('div.caldera_ongkir_subdistrict_container').removeClass('hidden')
      }
    })
  }

  function caldera_ongkir_notif (msg) {
    var notif = jQuery('.caldera_ongkir_notification')
    if (null === msg) notif.html('').hide()
    else notif.html(msg).show()
  }

})