<?php 
/**
 * Plugin Name: Caldera Ongkir
 * Plugin URI:  http://google.com
 * Description: Add on Caldera Form for calculating ongkos kirim
 * Version:     1.0.0
 * Author:      Henri 081901088918
 * Author URI:  https://bitbucket.org/liemgioktian/
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

define('CALDERA_ONGKIR_PATH',  plugin_dir_path( __FILE__ ));
define('CALDERA_ONGKIR_URL', plugin_dir_url(__FILE__));
define('CALDERA_ONGKIR_LOCAL_API', site_url('wp-json/caldera-ongkir/getData'));

register_activation_hook( __FILE__, function () {
  global $wpdb;
  $queries = array();
  $queries[] = "
    CREATE TABLE `caldera_ongkir_province` (
      `province_id` int(11) NOT NULL,
      `province` varchar(255) NOT NULL DEFAULT ''
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
  ";
  $queries[] = "
    ALTER TABLE `caldera_ongkir_province`
    ADD PRIMARY KEY (`province_id`)
  ";
  $queries[] = "
    ALTER TABLE `caldera_ongkir_province`
    MODIFY `province_id` int(11) NOT NULL AUTO_INCREMENT
  ";
  $queries[] = "
    CREATE TABLE `caldera_ongkir_city` (
    `city_id` int(11) NOT NULL,
    `city_name` varchar(255) NOT NULL DEFAULT '',
    `type` varchar(255) NOT NULL,
    `province_id` int(11) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8
  ";
  $queries[] = "
    ALTER TABLE `caldera_ongkir_city`
    ADD PRIMARY KEY (`city_id`)
  ";
  $queries[] = "
    ALTER TABLE `caldera_ongkir_city`
    MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT
  ";
  $queries[] = "
    CREATE TABLE `caldera_ongkir_subdistrict` (
      `subdistrict_id` int(11) NOT NULL,
      `subdistrict_name` varchar(255) NOT NULL,
      `city_id` int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
  ";
  $queries[] = "
    ALTER TABLE `caldera_ongkir_subdistrict`
    ADD PRIMARY KEY (`subdistrict_id`)
  ";
  $queries[] = "
    ALTER TABLE `caldera_ongkir_subdistrict`
    MODIFY `subdistrict_id` int(11) NOT NULL AUTO_INCREMENT
  ";
  $queries[] = "
    CREATE TABLE `caldera_ongkir_cost` (
      `id` int(11) NOT NULL,
      `origin` int(11) NOT NULL,
      `destination` int(11) NOT NULL,
      `courier` varchar(255) NOT NULL,
      `service` varchar(255) NOT NULL,
      `weight` varchar(255) NOT NULL,
      `result` text NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
  ";
  $queries[] = "
    ALTER TABLE `caldera_ongkir_cost`
    ADD PRIMARY KEY (`id`)
  ";
  $queries[] = "
    ALTER TABLE `caldera_ongkir_cost`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
  ";
  foreach ($queries as $query) $wpdb->query($query);
});

add_action('admin_menu', function () {
  add_submenu_page(
    'caldera-forms', 
    'Caldera Ongkir', 
    '<span class="caldera-forms-menu-dashicon"><span class="dashicons dashicons-cart"></span>Ongkir</span>',
    'manage_options', 
    'caldera-ongkir-settings', 
    function () {
      include CALDERA_ONGKIR_PATH . 'config.php';
    }
  );
});

add_filter ('caldera_forms_get_field_types' , function ($array) {
  $caldera_ongkir_fields = array('province', 'city', 'subdistrict', 'cost');
  foreach ($caldera_ongkir_fields as $field) {
    $array["caldera_ongkir_$field"] = array(
      "field"      => __( "Caldera Ongkir " . ucfirst($field), 'caldera-forms' ),
      "description"=> __( "Caldera Ongkir " . ucfirst($field), 'caldera-forms' ),
      'icon'       => CFCORE_URL . 'assets/build/images/calculator.svg',
      "file"       => CALDERA_ONGKIR_PATH . "field.php",
      "category"   => __( 'eCommerce', 'caldera-forms' ),
      "options"    => "single",
      "static"     => true,
      "viewer"     => array( Caldera_Forms::get_instance(), 'filter_options_calculator' ),
      "setup"      => array(
        "template" => CFCORE_PATH . "fields/dropdown/config_template.php",
        "preview"  => CFCORE_PATH . "fields/dropdown/preview.php",
        "default"  => array(),
      )
    );
  }
  $array['caldera_ongkir_cost']['scripts'][]= CFCORE_URL . "fields/select2/js/select2.min.js";
  $array['caldera_ongkir_cost']['scripts'][]= CALDERA_ONGKIR_URL . 'caldera-ongkir.js';
  $array['caldera_ongkir_cost']['styles'][] = CFCORE_URL . "fields/select2/css/select2.css";
  return $array;
}, 10, 1);

add_action('rest_api_init', function () {
  register_rest_route( 'caldera-ongkir', '/getData/', array(
      'methods'   => 'GET',
      'callback'  => 'calderaOngkirGetAPIData',
    )
  ); 
});

function calderaOngkirGetAPIData ($req) {
  global $wpdb;
  $param = $req->get_params();
  switch ($param['entity']) {
    case 'province':
      $records = $wpdb->get_results("SELECT * FROM caldera_ongkir_province");
      if (empty ($records)) {
        $records = rajaOngkirAPI (array(CURLOPT_URL => 'province'));
        foreach ($records as $record) {
          $wpdb->insert('caldera_ongkir_province', array(
            'province_id' => $record->province_id,
            'province'    => $record->province,
          ));          
        }
      }
      return $records;
      break;
    case 'city':
      $records = $wpdb->get_results("SELECT * FROM caldera_ongkir_city WHERE province_id=".$param['province']);
      if (empty ($records)) {
        $records = rajaOngkirAPI (array(CURLOPT_URL => 'city?province=' . $param['province']));
        foreach ($records as $record) {
          $wpdb->insert('caldera_ongkir_city', array(
            'city_id'     => $record->city_id,
            'city_name'   => $record->city_name,
            'type'        => $record->type,
            'province_id' => $record->province_id,
          ));
        }
      }
      return $records;
      break;
    case 'subdistrict':
      $records = $wpdb->get_results("SELECT * FROM caldera_ongkir_subdistrict WHERE city_id=".$param['city']);
      if (empty ($records)) {
        $records = rajaOngkirAPI (array(CURLOPT_URL => 'subdistrict?city=' . $param['city']));
        foreach ($records as $record) {
          $wpdb->insert('caldera_ongkir_subdistrict', array(
            'subdistrict_id'   => $record->subdistrict_id,
            'subdistrict_name' => $record->subdistrict_name,
            'city_id'          => $record->city_id,
          ));
        }
      }
      return $records;
      break;
    case 'cost':
      $origin = get_option('caldera-ongkir-origin-subdistrict');
      $dest   = $param['subdistrict'];
      $courier= 'jne';
      $service= 'REG';
      $weight = 1000;
      $result   = '[]';

      $query    = "
        SELECT * FROM caldera_ongkir_cost 
        WHERE origin = $origin AND destination = $dest
        AND courier = '$courier' AND service = '$service' AND weight = $weight
      ";

      $apiparam = array(
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_URL           => 'cost',
        CURLOPT_POSTFIELDS    => "origin=$origin&originType=subdistrict&destination=$dest&destinationType=subdistrict&courier=$courier&weight=$weight"
      );

      $insert         = array(
        'origin'      => $origin,
        'destination' => $dest,
        'courier'     => $courier,
        'service'     => $service,
        'weight'      => $weight,
        'result'      => $result
      );

      $record = $wpdb->get_row($query);
      if (!is_null ($record)) {
        $result = json_decode($record->result);
        if (isset ($result[0]) && isset ($result[0]->costs) && !empty($result[0]->costs)) {}
        else $result = getApiThenInsertIntoDB($apiparam, $insert, $record->id);
      } else $result = getApiThenInsertIntoDB($apiparam, $insert, null);

      if (empty ($result)) $cost = json_decode('{"error": "Gangguan koneksi, ulangi beberapa saat lagi"}', false);
      else $cost = json_decode('{"error": "'.strtoupper($courier).' '.$service.' untuk alamat anda tidak ditemukan, coba alamat lain"}', false);
      foreach ($result[0]->costs as $option) if ($option->service == $service) $cost = json_decode('{"value": '.$option->cost[0]->value.'}', false);
      return $cost;
      break;
  }
}

function getApiThenInsertIntoDB ($apiparam, $insert, $existingId) {
  global $wpdb;
  $record        = rajaOngkirAPI ($apiparam);
  $insert['result']= json_encode($record);
  if (!is_null ($existingId)) $wpdb->delete('caldera_ongkir_cost', array( 'id' => $existingId));
  $wpdb->insert('caldera_ongkir_cost', $insert);
  return $record;
}

function rajaOngkirAPI ($opt = array()) {
  $curl       = curl_init();
  $api_url    = 'http://pro.rajaongkir.com/api/';
  $optDefault = array(
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_HTTPHEADER => array(
      'content-type: application/x-www-form-urlencoded',
      'key: ' . get_option('caldera-ongkir-apikey')
    ),
  );
  if ( strpos($opt[CURLOPT_URL], $api_url) === false) $opt[CURLOPT_URL] = $api_url . $opt[CURLOPT_URL];
  foreach ($optDefault as $key => $value) {
    if (!isset($opt[$key])) $opt[$key] = $value;
  }
  curl_setopt_array($curl, $opt);
  $response = curl_exec($curl);
  $err = curl_error($curl);
  curl_close($curl);
  $response = json_decode($response, false);
  return 200 == $response->rajaongkir->status->code ? $response->rajaongkir->results : array();
}